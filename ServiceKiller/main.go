package main

import (
    "strings"
	"fmt"
    "encoding/json"
    "os"
    "os/exec"
    "strconv"
)

const cfg_file = "services.json"
// json file : map with map["binary_path"]"service_unit_name"

type Configuration map[string]string

func getServices() Configuration {
    file, _ := os.Open(cfg_file)
    defer file.Close()
    decoder := json.NewDecoder(file)
    cfg := Configuration{}
    err := decoder.Decode(&cfg)
    if err != nil {
      fmt.Println("getServices error:", err)
      return make(Configuration)
    }
    return cfg
}

func cmdOutput(strCommand string, args []string) string {
	out, _ := exec.Command(strCommand, args...).Output()
    if out != nil {
      return string(out)
    }
    return ""
}

func pidof(app_path string) string {
  slice_args := []string{app_path,}
  str_cmd := "/bin/pidof"
  return cmdOutput(str_cmd, slice_args)
}

func getInt(str_date string) int {
  number, _ :=  strconv.Atoi(strings.TrimSpace(str_date))
  return number
}

//Age in second
//expr $(date +%s) - $(stat --format=%Y /proc/1234)
func getage(pidof string) int {
  date      := cmdOutput("date", []string{"+%s",})
  int_date  := getInt(date)
  svc_time  := cmdOutput("stat", []string{"--format=%Y", "/proc/"+strings.TrimSpace(pidof)})
  int_svc   := getInt(svc_time)
  return int_date - int_svc
}

// main idea is to turn off socket-activated services .
// get pidof , then getage of pid, then if age is older than 
// execute systemctl stop service.
// do all this in an infinite loop every 5 min.
func main() {
    services := getServices()
    for binary, unit := range services {
      service_pidof := pidof(binary)
      if service_pidof != "" {
          //find out its age...
          fmt.Println(unit)
          service_age := getage(service_pidof)
          fmt.Println(service_age)
      }
      //fmt.Printf("%s ... %s \n", unit, pidof(binary))
    }
}
