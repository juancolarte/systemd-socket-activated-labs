Socket Activated Service

1. copy 404.service and 404.socket in /lib/systemd/system/
2. systemctl daemon-reload
3. systemctl enable 404.socket
4. systemctl start 404.socket

then you can start querying the service : curl http://localhost:8080/ 
it would automatically start the service ...

Note that without enabling the 404.service it does not start at boot.

Reference: https://vincent.bernat.ch/en/blog/2018-systemd-golang-socket-activation
