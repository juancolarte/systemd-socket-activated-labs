Socket Activated Service

Configuration uses systemd-socket-proxyd, for accesing normal tcp service.

1. copy actual.service, socket-activate-actual.service, socket-activate-actual.socket in /lib/systemd/system/
2. systemctl daemon-reload
3. systemctl enable socket-activate-actual.socket
4. systemctl start socket-activate-actual.socket

then you can start querying the service : curl http://localhost:3002/ 
it would automatically start the service ...

Note that without enabling the service it does not start at boot.

Reference: https://insanity.industries/post/socket-activation-all-the-things/